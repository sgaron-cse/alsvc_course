#!/usr/bin/env python
import os


def install(alsi):

    alsi.pip_install_all([
        'safepass',
    ])

    demo_dir = "/opt/al/var/course_demo"
    demo_cfg = os.path.join(demo_dir, 'conf')
    alsi.runcmd("mkdir -p {dir}".format(dir=demo_dir))
    alsi.runcmd("echo 'version: 1' > {conf}".format(conf=demo_cfg))
    alsi.runcmd("chmod 600 {conf}".format(conf=demo_cfg))


if __name__ == '__main__':
    from assemblyline.al.install import SiteInstaller
    install(SiteInstaller())
