"""
CourseDemo Service
See README.md for details about this service.
"""
import os

from assemblyline.al.service.base import ServiceBase
from assemblyline.al.common.result import Result, ResultSection, SCORE


class CourseDemo(ServiceBase):
    SERVICE_CATEGORY = 'Static Analysis'
    SERVICE_DESCRIPTION = "Demo service for Assemblyline course"
    SERVICE_REVISION = ServiceBase.parse_revision('$Id: 45f81478e892838f8ebb3a91b8e9aab74db587af $')
    SERVICE_VERSION = '1'
    SERVICE_TIMEOUT = 10
    SERVICE_ENABLED = False
    SERVICE_CPU_CORES = 0.1
    SERVICE_RAM_MB = 64

    def start(self):
        self.log.debug("CourseDemo service started")

    def execute(self, request):
        from safepass import safepass

        demo_dir = "/opt/al/var/course_demo"
        demo_cfg = os.path.join(demo_dir, 'conf')
        with open(demo_cfg, 'rb') as fh:
            if fh.read().strip() != "version: 1":
                raise ValueError("Wrong version in conf file...")

        result = Result()
        request.result = result
        section = ResultSection(SCORE.NULL, "Service ran successfully on the file")
        result.add_section(section)

